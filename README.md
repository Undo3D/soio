# Soio

A frontend template which lets you switch between React, Vue and Web Components


#### ESLint and React for Atom users

```bash
$ cd ~/.atom/packages/linter-eslint
$ npm i eslint-plugin-react
```
...from https://github.com/eslint/eslint/issues/3782#issuecomment-211169781
