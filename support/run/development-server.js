//// Load some of Node’s built in modules, and set some constants.
const
    { exec } = require('child_process'),
    { readFileSync, existsSync } = require('fs'),
    { join } = require('path'),
    port = process.env.PORT || 3000, // Heroku sets the environment $PORT value
    dir = join(__dirname, '..', '..')




//// ONE-SHOT SERVER

const firstP = process.argv.indexOf('-p')
const firstPath = process.argv.indexOf('--path')
const devReact = process.argv.includes('--dev-react')
const devVue = process.argv.includes('--dev-vue')

if (0 <= firstP || 0 <= firstPath) {

    if (0 <= firstP && 0 <= firstPath) throw Error('Use -p or --path, not both')
    const url = process.argv[ 0 <= firstP ? firstP+1 : firstPath+1 ]

    serveFile (
        { url }, // req
        { // res
            writeHead () {},
            end (content) {
                process.stdout.write(content+'')
                process.exit(0) //@TODO use error code from content
            },
        },
        { devReact, devVue },
    )

}




//// LONG-RUNNING SERVER

//// Load some of Node’s built in modules, and set some constants.
const app = require('http').createServer(server)

//// Shut down if the server hits an error. This wouldn’t be good in production!
app.on('error', e => {
    console.error('Server error: ' + e.message)
    app.close()
})

//// Start the server and open a browser window.
app.listen( port, () => {
    console.log(`Server is listening on port ${port}`)
    exec(`open http://localhost:${port}/`) //@TODO test in Windows and Linux
})




//// SERVE

//// Serve the proper response.
function server (req, res) {

    //// Any GET request is a static file.
    if ('GET' === req.method)
        return serveFile(req, res)

    //// Anything else is an error.
    return error(res, 9300, 405, 'Use GET')
}


//// Serve a file.
function serveFile (req, res, options={}) {

    //// Serve a request for the homepage.
    if ('/' === req.url) {
        res.writeHead(200, { 'Content-Type':'text/html' })
        res.end( readFileSync( join(dir, req.url, 'index.html') ) )
        return
    }

    //// Get the extension, MIME type and absolute path.
    const
        ext = (req.url.match(/\.([a-z]{2,4}2?|webmanifest)$/) || [])[1]
      , mime = extToMimeType(ext)
      , path = join(dir, req.url)
    if (! ext)  return error(res, 9101, 404, 'Invalid extension')
    if (! mime) return error(res, 9102, 404, `Extension .${ext} not recognised`)

    //// Check that the resource exists.
    if (! existsSync(path) )
        return error(res, 9100, 404, 'No such resource, ' + req.url)

    let content = readFileSync(path)

    //// Transform JSX to a React object.
    if ('jsx' === ext)
        try {
            content = reactify( nestTags( xmlParts(content) ) )
        } catch (err) {
            content = `console.error(${enquote(err.message)})`
        }

    //// Swap development for production lib/**/*.js files, for...
    if (devVue) // Vue if the '--dev-vue' flag is present.
        content = (content+'').replace(
            /\.\/lib\/vue\/vue\.development\.es6\.js/g,
            './lib/vue/vue.production.es6.min.js'
        )
    else if (devReact) // React if the '--dev-react' flag is present.
        content = (content+'').replace(
            /\.\/lib\/react\/react\.development\.es6\.js/g,
            './lib/react/react.production.es6.min.js'
        ).replace(
            /\.\/lib\/react\/react-dom\.development\.es6\.js/g,
            './lib/react/react-dom.production.es6.min.js'
        )

    //// Serve the request.
    res.writeHead(200, { 'Content-Type':mime })
    res.end(content)
}




//// UTILITY

//// Sends a response after a request which failed.
function error (res, code, status, remarks, contentType='text/plain') {
    const headers = { 'Content-Type': contentType }
    remarks = remarks.replace(/\\/g, '\\\\')
                     .replace(/"/g, '\\"')
                     .replace(/\n/g, '\\n')
    res.writeHead(status, headers)
    res.end(`{ "code":${code}, "error":"${remarks}", "status":${status} }\n`)
    return false
}

//// Returns a given extension’s MIME type, or undefined if not recognised.
function extToMimeType (ext) {
    return ({
        js:   'application/javascript' // a standard JavaScript script
      , jsx:  'application/javascript' // eg during development of a React app
      , mjs:  'application/javascript' // Node.js’s preferred extension for ES6
      , json: 'application/json'       // JSON data, eg package.json
      , mem:  'application/wasm'       // asm.js memory, used by threecap.js.mem @TODO proper MIME type?
      , wasm: 'application/wasm'       // WebAssembly memory
      , mp3:  'audio/mpeg'             // MP3 audio
      , ttf:  'font/ttf'               // TrueType Font
      , otf:  'font/otf'               // OpenType Font
      , woff: 'font/woff'              // Web Open Font Format
      , woff2:'font/woff2'             // Web Open Font Format 2.0
      , gif:  'image/gif'              // GIF image
      , jpg:  'image/jpeg'             // JPEG image
      , jpeg: 'image/jpeg'             // JPEG image
      , png:  'image/png'              // PNG image
      , svg:  'image/svg+xml'          // SVG image
      , ico:  'image/x-icon'           // usually favicon.ico
      , css:  'text/css'               // CSS stylesheet
      , htm:  'text/html'              // HTML page
      , html: 'text/html'              // HTML page
      , xml:  'text/xml'               // XML readable by casual users
      , txt:  'text/plain'             // plain text document
      , webmanifest: 'text/plain'      // support/asset/icon/site.webmanifest
      , mp4:  'video/mp4'              // MP4 video
      , webm: 'video/webm',            // WEBM video
    })[ext]
}




//// JSX

//// xmlParts() is the first step in converting JSX to React functions. It’s
//// also called recursively by nestTags(). It converts a string to an array of
//// objects, where the objects are one of two types:
////   { text:'Plain JavaScript code here' }
////   { matchOpen:'h1 id="foo"', matchClose:'h1', text:'Some Headline' }
function xmlParts (content) {
    content += '' // convert to string, if it came from `readFileSync()`
    const parts = [{ text:'' }]
    let isInXML = false

    for (let i=0, l=content.length; i<l; i++) {
        const char = content[i]
        const part = parts[parts.length-1]

        // Deal with an opening-tag, eg <DIV>.
        const matchOpen = content.slice(i).match(/^<([^/>]+)>/)
        if (matchOpen) {
            if (isInXML) {
                part.text += matchOpen[0]
                part.depth++
            } else {
                parts.push({ matchOpen:matchOpen[1], depth:0, text:'' })
                isInXML = true
            }
            i += matchOpen[0].length - 1
            continue
        }

        // Deal with a closing-tag, eg </DIV>.
        const matchClose = content.slice(i).match(/^<\/([-_a-z0-9]+)>/i)
        if (matchClose) {
            if (0 < part.depth) {
                part.text += matchClose[0]
                part.depth--
            } else {
                part.matchClose = matchClose[1]
                parts.push({ text:'' })
                isInXML = false
            }
            i += matchClose[0].length - 1
            continue
        }

        // Treat a self-closing tag like <BR/> as if it was <BR></BR>.
        const matchSelf = content.slice(i).match(/^<([-_a-z0-9]+[^/>]+)\/>/i)
        if (matchSelf) {
            if (0 < part.depth) {
                part.text += matchSelf[0]
            } else {
                parts.push({
                    matchOpen: matchSelf[1], // include any attributes here...
                    matchClose: matchSelf[1].match(/^[-_a-z0-9]+/i)[0], // ...but not here
                    text: '',
                })
                parts.push({ text:'' })
            }
            i += matchSelf[0].length - 1
            continue
        }

        // Anything else just gets passed along unchanged.
        part.text += char
    }
    return parts
}


//// Converts the result
function nestTags (parts) {
    return parts.map( ({ matchOpen, text, matchClose }) => {
        if (! matchOpen) return { text } // is regular JavaScript
        const children = nestTags( xmlParts(text) )
            .filter( c => '' !== c.text)
        const props = getProps(matchOpen, matchClose)
        return {
            type: matchClose,
            props,
            children: children.length ? children : null,
        }
    })
}


function getProps (open, close) {

    // Shortcut for trivial props.
    if (open === close) return null

    // Retrieve the attributes as a raw string.
    const matchAttrs = open.match( new RegExp(`^${close}\\s+(.*)`) )
    if (! matchAttrs) throw Error(`Maybe mismatch '${open}' vs '${close}'`)
    const attrs = matchAttrs[1] + ' ' // + ' ' regularises treatment of last char

    const props = [{ }]
    let awaitingKey = true
    let isInKey = false
    let awaitingEquals = false
    let awaitingValue = false
    let isInValue = false
    let isInDQs = false // double quotes
    let isInSQs = false // single quotes
    let isInCBs = false // curly braces

    for (let i=0, l=attrs.length+1; i<l; i++) {
        const char = attrs[i] || ' '
        const prop = props[props.length-1]
        if (awaitingKey) {
            if (' ' !== char) {
// console.log(1, char);
                awaitingKey = false
                isInKey = true
                prop.key = char
            }
        } else if (isInKey) {
            if (' ' === char) {
// console.log(2, char);
                isInKey = false
                awaitingEquals = true
            } else if ('=' === char) {
// console.log(3, char);
                isInKey = false
                awaitingValue = true
            } else {
// console.log(4, char);
                prop.key += char
            }
        } else if (awaitingEquals) {
            if ('=' === char) {
// console.log(5, char);
                awaitingEquals = false
                awaitingValue = true
            } else if (' ' !== char) {
// console.log(6, char);
                awaitingEquals = false
                isInKey = true
                prop.value = true
                props.push({ key:char })
            }
        } else if (awaitingValue) {
            if ('"' === char) {
// console.log(7, char);
                awaitingValue = false
                isInValue = true
                isInDQs = true
                prop.value = ''
            } else if ("'" === char) {
// console.log(8, char);
                awaitingValue = false
                isInValue = true
                isInSQs = true
                prop.value = ''
            } else if ("{" === char) {
// console.log(9, char);
                awaitingValue = false
                isInValue = true
                isInCBs = true
                prop.value = ''
            } else if (' ' !== char) {
// console.log(10, char);
                awaitingValue = false
                isInValue = true
                prop.value = char
            }
        } else if (isInValue) { //@TODO deal with backslash, escaped quotes
            if ('"' === char && isInDQs) {
// console.log(11, char);
                isInValue = false
                isInDQs = false
                awaitingKey = true
                prop.value = `"${prop.value}"`
                props.push({ })
            } else if ("'" === char && isInSQs) {
// console.log(12, char);
                isInValue = false
                isInSQs = false
                awaitingKey = true
                prop.value = `'${prop.value}'`
                props.push({ })
            } else if ('}' === char && isInCBs) {
// console.log(13, char);
                isInValue = false
                isInCBs = false
                awaitingKey = true
                props.push({ })
            } else if ( ' ' === char && (! isInSQs && ! isInDQs && ! isInCBs) ) {
// console.log(14, char);
                isInValue = false
                awaitingKey = true
                prop.value = enquote(prop.value)
                props.push({ })
            } else {
// console.log(15, char);
                prop.value += char
            }
        }
    }

    const prop = props[props.length-1]
    if (awaitingValue)
        throw Error(`props end in '=': ${attrs}`)
    if (isInKey || awaitingEquals)
        prop.value = true

    if (! prop.key)
        return props.slice(0, -1)
    return props
}


function enquote (val) {
    if ( 0 > val.indexOf("'") )
        return `'${val}'`
    if ( 0 > val.indexOf('"') )
        return `"${val}"`
    if ( 0 > val.indexOf('`') )
        return '`'+val+'`'
    throw Error('@TODO smarter quotes')
}


function reactify (nested, depth=0) {
    const out = nested.reduce( (output, { type, text, props, children }) => {
        const pad = ' '.repeat(depth*4)

        // Deal with text.
        if (! type)
            if ('' === text) // remove empty strings
                return output
            else if (0 === depth) // keep as-is on the top-level
                return [ ...output, text ]
            else //  inside XML
                return [ ...output, ...formatText(text, pad) ]
        return [ ...output,
`${pad}React.createElement(
${pad}    ${firstIsUc(type) ? type : "'" + type + "'"},
${pad}    ${props ? formatProps(props, pad) : 'null'},
${children ? reactify(children, depth+1).join(',\n') : pad+'    null'}
${pad})`]
    }, [])
    // console.log( out[0].join('\n') )
    if (0 === depth)
        return out.join('')
    return out
}


function firstIsUc (text) {
    return 0 <= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(text[0])
}

function formatProps (props, pad) {
    pad += '    '
    return `{ ` +
        props.map( ({ key, value }) => `${key}: ${value},\n`).join(`${pad}  `) +
        `${pad}}`
}

function formatText (text, pad) {

    // Shortcut for trivial `text`.
    const firstPos = text.indexOf('{')
    if (0 > firstPos)
        return [ pad + escapeNLs( enquote(text) ) ]

    // Split into an array of ‘text’ and ‘js’ fragments.
    const fragments = [{ text:text.slice(0, firstPos) }] // we got this already!
    for (let i=firstPos, l=text.length; i<l; i++) {
        const char = text[i]
        const fragment = fragments[fragments.length-1]
        if ('{' === char) {
            if (fragment.isJs) {
// console.log(1, char)
                fragment.js += char
                fragment.depth++
            } else {
// console.log(2, char)
                fragments.push({ depth:1, js:'', isJs:true })
            }
        } else if ('}' === char) { //@TODin a JS string? comment? escaped in a string?!
            if (fragment.isJs) {
                if (1 === fragment.depth) {
// console.log(3, char)
                    fragments.push({ text:'' })
                } else {
// console.log(4, char)
                    fragment.depth--
                    fragment.js += char
                }
            } else {
// console.log(5, char)
                fragment.text += char
            }
        } else {
            if (fragment.isJs) {
// console.log(6, char)
                fragment.js += char
            } else {
// console.log(7, char)
                fragment.text += char
            }
        }
    }

    // Last fragment should be text (maybe ''). Otherwise JS didn’t close right.
    const lastFragment = fragments[fragments.length-1]
    if (lastFragment.isJs)
        throw Error(`Unbalanced curly braces: ${lastFragment.js}`)

    // Convert to a simple array of quoted and unquoted strings.
    return fragments
        .map(({ text, js, isJs }) => isJs ? js : escapeNLs( enquote(text) ) )
        .filter(f => "''" !== f)
        .map(f => pad + f )
}


function escapeNLs (text) {
    return text.replace(/\n/g, '\\n')
}

/*
const $pre = document.querySelector('pre')
// const testJSX = 'ok 123 = <hey a="ok yep"   fgdgdg    yttyu>yup!<i inner=123><br/>oh</i></hey> more <b>heh</b> ok'
// const testJSX = 'start <l1> a <L2> b <a> c </a> d </L2> e <L3 a=1> b <a></a> d </L3> </l1> end'
// const testJSX = 'start <div data={[5, 4, 2]}></div> end'
// const testJSX = 'return <div data={10 - 5}><b>bold</b>t{h}i}ng{here{}}<i>italic</i></div> end'
const testJSX = 'return <br ok=123 heh bb=8/> end'

$pre.innerHTML += testJSX.replace(/</g, '&lt;') + '\n' + '\n\n'

$pre.innerHTML += JSON.stringify(
    xmlParts(testJSX),
    null, 2
).replace(/</g, '&lt;') + '\n\n'

$pre.innerHTML += JSON.stringify(
    nestTags( xmlParts(testJSX) ),
    null, 2
).replace(/</g, '&lt;') + '\n\n'

$pre.innerHTML +=
    reactify( nestTags( xmlParts(testJSX) ) )
.replace(/</g, '&lt;') + '\n'
*/
