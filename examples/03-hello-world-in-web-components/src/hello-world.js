export default class HelloWorld extends HTMLParagraphElement {
    constructor() {
        super()
        const shadow = this.attachShadow({ mode:'open' })
        this.message = document.createElement('h1')
        this.message.textContent = this.getAttribute('text')
        shadow.appendChild(this.message)
    }
    attributeChangedCallback () { // name, oldValue, newValue
        this.message.textContent = this.getAttribute('text')
    }
    static get observedAttributes() { return ['text'] }
}
