//// Load Node’s filesystem library.
const fs = require('fs')

//// Recursively drills down through folder levels, deleting all .DS_Store files.
function listDSStorePaths (path) {
    fs.readdirSync(path).forEach( name => {
        if ('.DS_Store' === name)
            fs.unlinkSync(path + '/' + name)
        else if ('LICENSE' === name) // not a directory!
            return
        else if ( -1 === name.indexOf('.') ) {
            listDSStorePaths(path + '/' + name)
        }
    })

}
listDSStorePaths('./')
