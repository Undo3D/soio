// Define a ‘local’ component.
export default {
    props: ['text'],
    template: `
      <div>
        <h1>{{ text }}</h1>
      </div>
    `,
}
