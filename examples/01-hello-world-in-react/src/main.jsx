import React from '../../../lib/react/react.development.es6.js'
import ReactDOM from '../../../lib/react/react-dom.development.es6.js'
import HelloWorld from './hello-world.jsx'

/* A one-liner, you can use inline js in html if you like */
ReactDOM.render(
    <HelloWorld text="Hello React" />,
    document.getElementById('content')
)
