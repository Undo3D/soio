import React from '../../../lib/react/react.development.es6.js'

export default class HelloWorld extends React.Component {
    static get propTypes() {
        return {
            text: () => null,
        };
    }
    render () {
        return (
          <div>
            <h1>{ this.props.text }</h1>
          </div>
        )
    }
}
