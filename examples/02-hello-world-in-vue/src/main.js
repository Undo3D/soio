import Vue from '../../../lib/vue/vue.development.es6.js'
import HelloWorld from './hello-world.js'

new Vue({
    el: '#content',
    components: {
        'hello-world': HelloWorld,
    },
})

// This would work just as well:
// new Vue({
//     el: '#content',
//     components: {
//         HelloWorld,
//     },
// })
